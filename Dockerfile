FROM openjdk:8-jdk-alpine
EXPOSE 8086
ADD target/reservation-service-0.0.1-SNAPSHOT.jar reservation-reservation-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/reservation-service-0.0.1-SNAPSHOT.jar"]
